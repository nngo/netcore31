using AutoMapper;
using netcore31.Dtos.Character;
using netcore31.Dtos.Weapon;
using netcore31.Models;

namespace netcore31
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Character, GetCharacterDto>();
            CreateMap<AddCharacterDto, Character>();
            CreateMap<Weapon, GetWeaponDto>();
        }
    }
}