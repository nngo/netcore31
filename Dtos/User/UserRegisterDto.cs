namespace netcore31.Dtos.User
{
    public class UserRegisterDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}