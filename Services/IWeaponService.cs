using System.Threading.Tasks;
using netcore31.Dtos.Character;
using netcore31.Dtos.Weapon;
using netcore31.Models;

namespace netcore31.Services
{
    public interface IWeaponService
    {
        Task<ServiceResponse<GetCharacterDto>> AddWeapon(AddWeaponDto newWeapon);
    }
}